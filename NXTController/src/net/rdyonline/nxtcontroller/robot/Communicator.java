package net.rdyonline.nxtcontroller.robot;

import java.io.OutputStreamWriter;
import java.util.Set;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

public class Communicator {
	
	private final String 		NXT_NAME							= "NXT"; 		
	private static final UUID 	SERIAL_PORT_SERVICE_CLASS_UUID 		= UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	Context						androidContext						= null;
	BluetoothAdapter 			bluetoothAdapter 					= BluetoothAdapter.getDefaultAdapter();
	BluetoothDevice				device								= null;
	BluetoothSocket				socket								= null;
	
	public Communicator(Context context) throws Exception {
		
		androidContext 				= context;
		
		if (bluetoothAdapter != null) {
		    // Device supports Bluetooth - should be the case for the nexus 7!
			
			if (!bluetoothAdapter.isEnabled()) {
			    throw new Exception("Bluetooth needs to be enabled");
			}
			
			Set<BluetoothDevice> 	pairedDevices 		= bluetoothAdapter.getBondedDevices();
			
			// If there are paired devices
			if (pairedDevices.size() > 0) {
			    // Loop through paired devices
			    for (BluetoothDevice device : pairedDevices) {
			        // Add the name and address to an array adapter to show in a ListView
			    	
			    	if (device.getName().trim().equals(NXT_NAME)) {
			    		// found the NXT device
			    		socket 							= device.createRfcommSocketToServiceRecord(SERIAL_PORT_SERVICE_CLASS_UUID);
			    		socket.connect();
			    		break;
			    	}
			    }
			}
		}
	}
	
	public void writeMessage(byte msg) throws Exception {
	    
		try {
			OutputStreamWriter 	out 		= new OutputStreamWriter(socket.getOutputStream());
			
			out.write(msg);
			out.flush();
			
		} catch (Exception e) {
			Log.e("Communicator", e.getMessage());
		}
	}
	
	public void requestStart() {
		try {
			writeMessage(Integer.valueOf(2).byteValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void requestStop() {
		try {
			writeMessage(Integer.valueOf(1).byteValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
