package net.rdyonline.nxtcontroller.activity;

import net.rdyonline.nxtcontroller.R;
import net.rdyonline.nxtcontroller.robot.Communicator;
import net.rdyonline.nxtcontroller.view.CameraPreview;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CameraActivity extends Activity {

	private Camera 				mCamera				= null;
    private CameraPreview 		mPreview			= null;
    
    private PowerManager		powerManager		= null;
    private WakeLock			wakeLock			= null;
    
    private TextView			infoText			= null;
    
    Communicator				robotComm			= null;
	
    private int 				faceGoneCount		= 3;
    private int 				faceGoneThreshold	= 3;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        // required to obtain a wake lock. Make sure the screen doesn't sleep when you are reading a password
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        
        setupViews();
        setupRobot();
    }
    
    @Override
    public void onBackPressed() {
    	super.onBackPressed();
    	
    	// close off the camera
    	if (mCamera != null) {
    		mCamera.stopPreview();
    		mCamera.release();
    	}
    }
	
    @Override
    protected void onResume() {
    	super.onResume();
    	//before we grab the camera, check to see if the lock screen is up
    	KeyguardManager 	myKM 		= (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
    	
    	if( !myKM.inKeyguardRestrictedInputMode()) {
    		mPreview.obtainCamera();
    	}
    	
        wakeLock				= powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "");    
        wakeLock.acquire();
    }
    
    @Override
    protected void onPause() {
        mPreview.releaseCamera();
        
		if (wakeLock != null) {
			try {
				wakeLock.release();
			} catch (Exception e) {
				// no need to release the wake lock if there isn't one
			}
		}
		
        super.onPause();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    private void setupViews() {
    	setContentView(R.layout.activity_main);
    	
    	infoText 					= (TextView) findViewById(R.id.overlay_control_info);
    	
    	// Create our Preview view and set it as the content of our activity.
        mPreview 					= new CameraPreview(this, mCamera);
        FrameLayout 	preview 	= (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview,0);
        
        //RelativeLayout layoutControls = (RelativeLayout) findViewById(R.id.overlay_controls);
        mPreview.setZOrderOnTop(false);
    }
    
    private void setupRobot() {
        try {
 			robotComm 					= new Communicator(this);
 			
 		} catch (Exception e) {
 			Toast.makeText(this, "Could not talk to the robot, check bluetooth", Toast.LENGTH_SHORT).show();
 			
 			finish();
 		}
    }
    
    public void setInfoText(String message) {
    	infoText.setText(message);
    }
    
    public void requestRobotStart() {
    	if (faceGoneCount < faceGoneThreshold) {
    		faceGoneCount++;
    	} else {
    		if (faceGoneCount == faceGoneThreshold) {
    			Toast.makeText(this, "start robot", Toast.LENGTH_SHORT).show();
    			faceGoneCount++;
    			robotComm.requestStart();
    		}
    	}
    }
    
    public void requestRobotStop() {
    	if (faceGoneCount > 0) {
    		Toast.makeText(this, "stop robot", Toast.LENGTH_SHORT).show();
    		
    		new Thread(new Runnable() {

				@Override
				public void run() {
					MediaPlayer mp = new MediaPlayer();
	        		try {
	    	    		AssetFileDescriptor afd = getAssets().openFd("ah_human.mp3");
	    	    		mp.setDataSource(afd.getFileDescriptor());
	    	    		afd.close();
	    	    		mp.prepare();
	    	    		mp.start();
	    	    		Thread.sleep(4000);
	    	    		
	    	            mp.stop();
	    	            mp.release();
	    	            mp = null;
	    	            
	        		} catch (Exception ex) {
	        			
	        		}
					
				}
    		}).start();
    		
            robotComm.requestStop();
    	} 
    	faceGoneCount = 0;
    }
}
