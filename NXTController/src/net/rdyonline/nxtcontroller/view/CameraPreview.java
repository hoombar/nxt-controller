package net.rdyonline.nxtcontroller.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import net.rdyonline.nxtcontroller.activity.CameraActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.FaceDetector;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

	private static final int 		NUM_FACES 				= 1; // max 64
	private static final boolean 	DEBUG 					= true;

	private SurfaceHolder 			mHolder					= null;
	private Camera 					mCamera					= null;
	private String 					TAG 					= "CameraPreview";
	private Bitmap 					mWorkBitmap				= null;

	private FaceDetector 			mFaceDetector			= null;
	private FaceDetector.Face[] 	mFaces 					= new FaceDetector.Face[NUM_FACES];
	private FaceDetector.Face 		face 					= null; 

	private Paint 					outerBox 				= new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint 					innerBox 				= new Paint(Paint.ANTI_ALIAS_FLAG);

	private PointF 					eyesMidPts[] 			= new PointF[NUM_FACES];
	private float  					eyesDistance[] 			= new float[NUM_FACES];

	private RectF 					flippedRect 			= new RectF();
	private RectF 					scaledRect 				= new RectF();

	private int						width					= 0;
	private int						height					= 0;
	
	CameraActivity					camActivity				= null;
	
	boolean							faceDetected			= false;
	int								noFaceCounter			= 0;
	
	@SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera) {
		super(context);

		camActivity 			= (CameraActivity) context;
		mHolder 				= getHolder();

		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mHolder.setFormat(ImageFormat.NV21);

		// used for drawing box around the face
		outerBox.setColor(Color.WHITE);
		outerBox.setStrokeCap(Cap.ROUND);
		outerBox.setStrokeWidth(10);
		outerBox.setStyle(Paint.Style.STROKE);
		
		innerBox.setColor(Color.RED);
		innerBox.setStyle(Paint.Style.FILL);
		innerBox.setAlpha(100);
	}

	public void obtainCamera() {
		// 0 for the nexus 7
		mCamera = Camera.open(0);

	}

	public void releaseCamera(){
		if(mCamera != null){
			mCamera.stopPreview();
			mCamera.setPreviewCallbackWithBuffer(null);
			mCamera.release();

			mCamera 			= null;
		}

		setWillNotDraw(true);
	}
	
	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, now tell the camera where to draw the preview.
		try {
			mCamera.setPreviewCallback(mPreviewCallback);
			mCamera.setPreviewDisplay(holder);

			mCamera.startPreview();	

		} catch (IOException e) { 
			Log.d(TAG, "Error setting camera preview: " + e.getMessage());
		}

		setWillNotDraw(false);
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// empty. Take care of releasing the Camera preview in your activity.
	}

	private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double 	ASPECT_TOLERANCE 			= 0.05;
		double 			targetRatio 				= (double) w / h;
		
		if (sizes == null) return null;

		Size 			optimalSize 				= null;
		double 			minDiff 					= Double.MAX_VALUE;

		int targetHeight = h;

		// Try to find an size match aspect ratio and size
		for (Size size : sizes) {
			double ratio = (double) size.width / size.height;
			
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
			
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize 						= size;
				minDiff 							= Math.abs(size.height - targetHeight);
			}
		}

		// Cannot find the one match the aspect ratio, ignore the requirement
		if (optimalSize == null) {
			minDiff 								= Double.MAX_VALUE;
			
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize 					= size;
					minDiff 						= Math.abs(size.height - targetHeight);
				}
			}
		}
		
		return optimalSize;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.

		if(mCamera != null){
			// Now that the size is known, set up the camera parameters and begin
			// the preview.
			// TODO: the obtained size is dependant on the current orientation, and that's bad
			Camera.Parameters 		parameters 			= mCamera.getParameters();
			List<Size> 				sizes 				= parameters.getSupportedPreviewSizes();
			Size 					optimalSize 		= getOptimalPreviewSize(sizes, w, h);
			
			parameters.setPreviewSize(optimalSize.width, optimalSize.height);

			mCamera.setParameters(parameters);
			mCamera.startPreview();

			// Setup the objects for the face detection
			mWorkBitmap 								= Bitmap.createBitmap(optimalSize.width, optimalSize.height, Bitmap.Config.RGB_565);
			width										= mWorkBitmap.getWidth();
			height										= mWorkBitmap.getHeight();
			mFaceDetector 								= new FaceDetector(optimalSize.width, optimalSize.height, NUM_FACES);

			int 					bufSize 			= optimalSize.width * 
															optimalSize.height * 
															ImageFormat.getBitsPerPixel(parameters.getPreviewFormat()) / 
															8;
			byte[] 					cbBuffer 			= new byte[bufSize];
			
			mCamera.setPreviewCallbackWithBuffer(mPreviewCallback);
			mCamera.addCallbackBuffer(cbBuffer);
		}
	}

	PreviewCallback mPreviewCallback = new PreviewCallback() {

		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			
			// face detection: first convert the image from NV21 to RGB_565
			YuvImage 				yuv 			= new YuvImage(data, ImageFormat.NV21, width, height, null);
			// TODO: make rect a member and use it for width and height values above
			Rect 					rect 			= new Rect(0, 0, width, height);	

			// TODO: use a threaded option or a circular buffer for converting streams?  
			//see http://ostermiller.org/convert_java_outputstream_inputstream.html
			ByteArrayOutputStream 	baout 			= new ByteArrayOutputStream();
			if (!yuv.compressToJpeg(rect, 100, baout)) {
				Log.e(TAG, "compressToJpeg failed");
			}
			
			BitmapFactory.Options 	bfo 			= new BitmapFactory.Options();
			bfo.inPreferredConfig 					= Bitmap.Config.RGB_565;
			mWorkBitmap 							= BitmapFactory.decodeStream(new ByteArrayInputStream(baout.toByteArray()), null, bfo);

			// use arraycopy instead?
			Arrays.fill(mFaces, null);	
			Arrays.fill(eyesMidPts, null);
			// not much point trying to optimise the rest of the code.
			// After performance profiling this, the below method (findFaces) was taking 70% of the time
			mFaceDetector.findFaces(mWorkBitmap, mFaces);

			for (int i = 0; i < mFaces.length; i++)
			{
				face 								= mFaces[i];
				try {
					PointF 			eyesMP 			= new PointF();
					face.getMidPoint(eyesMP);
					eyesDistance[i] 				= face.eyesDistance();
					eyesMidPts[i] 					= eyesMP;

					if (DEBUG)
					{
						Log.i("Face",
								i +  " " + face.confidence() + " " + face.eyesDistance() + " "
										+ "Pose: ("+ face.pose(FaceDetector.Face.EULER_X) + ","
										+ face.pose(FaceDetector.Face.EULER_Y) + ","
										+ face.pose(FaceDetector.Face.EULER_Z) + ")"
										+ "Eyes Midpoint: ("+eyesMidPts[i].x + "," + eyesMidPts[i].y +")"
								);
					}
				}
				catch (Exception e)
				{
					//if (DEBUG) Log.e("Face", i + " is null");
				}
			}

			invalidate();
			mCamera.addCallbackBuffer(data);
			
		}
		
	};


	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if(mWorkBitmap != null){

			camActivity.setInfoText("No faces found, seeking them out!!");
			faceDetected				= false;
			
			for (int i = 0; i < eyesMidPts.length; i++)
			{
				// make sure
				// - there is a face there
				// - that the face is reasonably close - this should rule out any false positives
				if (eyesMidPts[i] != null && eyesDistance[i] > 50)
				{
					float left, right;
					// for some reason on the nexus 7, the face was being returned on the opposing side of the image
					// this just flips the box on to the other side of the canvas
					if (eyesMidPts[i].x > mWorkBitmap.getWidth()/2) {
						// further than half way
						left 		= (mWorkBitmap.getWidth()/2) - (eyesMidPts[i].x - (mWorkBitmap.getWidth()/2)) - eyesDistance[i];
						right 		= (mWorkBitmap.getWidth()/2) - (eyesMidPts[i].x - (mWorkBitmap.getWidth()/2)) + eyesDistance[i];
					} else {
						// less than half way
						left 		= (mWorkBitmap.getWidth()/2) + ((mWorkBitmap.getWidth()/2) - eyesMidPts[i].x) - eyesDistance[i];
						right 		= (mWorkBitmap.getWidth()/2) + ((mWorkBitmap.getWidth()/2) - eyesMidPts[i].x) + eyesDistance[i];
					}
					scaledRect.set(	left,
							eyesMidPts[i].y - eyesDistance[i],
							right,
							eyesMidPts[i].y + eyesDistance[i]);

					flippedRect.set(scaledRect.right, scaledRect.top, scaledRect.left, scaledRect.bottom);

					canvas.drawRect(scaledRect, innerBox);
					canvas.drawRect(scaledRect, outerBox);
					
					noFaceCounter		= 0;
					faceDetected		= true;
					
					camActivity.requestRobotStop();
					camActivity.setInfoText("face found!");
				}
			}
		}
		
		if (!faceDetected) {
			noFaceCounter++;
			
			if (noFaceCounter > 3) {
				camActivity.requestRobotStart();
			}
		}
	}
}